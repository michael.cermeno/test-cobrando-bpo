const readlineSync = require('readline-sync');


const sumar = (a, b) => a + b
const restar = (a, b) => a - b
const multiplicar = (a, b) => a * b
const dividir = (a, b) => a / b


console.log('Calculadora')
const numeroA = Number(readlineSync.question('Escriba el numero A: '))
const numeroB = Number(readlineSync.question('Escriba el numero B: '))


console.log('Escoja la operación a realizar')
console.log('1. sumar')
console.log('2. restar')
console.log('3. multiplicar')
console.log('4. dividir')

const opcion = readlineSync.question('Escriba una opcion: ')

switch (opcion) {
    case '1':
        console.log(`Resultado ${sumar(numeroA, numeroB)}`)
        break;
    case '2':
        console.log(`Resultado ${restar(numeroA, numeroB)}`)
        break;
    case '3':
        console.log(`Resultado ${multiplicar(numeroA, numeroB)}`)
        break;
    case '4':
            console.log(`Resultado ${dividir(numeroA, numeroB)}`)
        break;

    default:
        console.log('intente nuevamente')
        break;
}