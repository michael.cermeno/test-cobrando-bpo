
const esNumeroPrimo = (numero) => {
    for (var i = 2; i < numero; i++) {

        if (numero % i === 0) {
            return false;
        }
    
    }
    
    return numero !== 1;
}

for (let index = 1; index <= 100; index++) {

    if (esNumeroPrimo(index))  console.log(`El número ${index} es primo`);
}