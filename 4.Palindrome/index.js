var readlineSync = require('readline-sync');

const esPalindrome = (palabra) => {
    const palabraReversa = palabra.toLowerCase().split('').reverse().join('')

    return palabra === palabraReversa
}




console.log('Palindrome')
const palabra = readlineSync.question('Escriba una palabra: ')

console.log(`La palabra ${palabra}${esPalindrome(palabra) ? '' : ' no'} es palindrome`)
